package sessions.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sessions.demo.model.User;

import java.util.Optional;

/**
 * @author Ali Dehghani
 */
public interface UserRepository extends JpaRepository<User, String> {

    Optional<User> findByUsername(String username);
}
