package sessions.demo;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import sessions.demo.model.User;
import sessions.demo.repository.UserRepository;

import java.util.Collections;
import java.util.Optional;

@SpringBootApplication
public class DemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Component
    public static class DbInitializer implements CommandLineRunner {

        private final UserRepository userRepository;
        private final PasswordEncoder passwordEncoder;

        public DbInitializer(UserRepository userRepository, PasswordEncoder passwordEncoder) {
            this.userRepository = userRepository;
            this.passwordEncoder = passwordEncoder;
        }

        @Override
        public void run(String... args) throws Exception {
            Optional<User> luke = userRepository.findByUsername("luke");
            if (!luke.isPresent()) {
                User user = new User();
                user.setFirstName("Luke");
                user.setLastName("Skywalker");
                user.setRoles(Collections.singleton("ROLE_ADMIN"));
                user.setUsername("luke");
                user.setPassword(passwordEncoder.encode("Padme"));

                userRepository.save(user);
            }
        }
    }
}
