package sessions.demo.conf;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import sessions.demo.model.User;
import sessions.demo.repository.UserRepository;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Ali Dehghani
 */
@Component
public class JpaUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    public JpaUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Username not found"));

        List<SimpleGrantedAuthority> roles = user.getRoles().
                stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());

        // Map our user model to Spring Security's user model
        return new org.springframework.security.core.userdetails.User(username, user.getPassword(), roles);
    }
}

