package sessions.demo.web;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Set;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY;
import static java.util.stream.Collectors.toSet;
import static org.springframework.security.web.context.HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY;

/**
 * @author Ali Dehghani
 */
@RestController
@RequestMapping("/auth")
public class AuthController {

    private final AuthenticationManager authenticationManager;

    public AuthController(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @PostMapping
    public AuthResponse authenticate(@RequestBody AuthRequest authRequest, HttpServletRequest request) {

        // Performing authentication
        Authentication response = authenticationManager.authenticate(authRequest.toRequestToken());
        SecurityContextHolder.getContext().setAuthentication(response);

        // Creating session
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute(SPRING_SECURITY_CONTEXT_KEY, SecurityContextHolder.getContext());

        return AuthResponse.of(response, httpSession.getId());
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void logout() {
        SecurityContextHolder.clearContext();
    }

    @JsonAutoDetect(fieldVisibility = ANY)
    private static final class AuthRequest {
        private String username;
        private String password;

        Authentication toRequestToken() {
            return new UsernamePasswordAuthenticationToken(username, password);
        }
    }

    @JsonAutoDetect(fieldVisibility = ANY)
    private static final class AuthResponse {
        private String token;
        private String username;
        private Set<String> roles;

        static AuthResponse of(Authentication authentication, String session) {
            AuthResponse response = new AuthResponse();
            response.username = authentication.getName();
            response.token = session;
            response.roles = authentication.getAuthorities()
                    .stream().map(GrantedAuthority::getAuthority).collect(toSet());

            return response;
        }
    }
}
