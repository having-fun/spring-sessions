package sessions.demo.web;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import sessions.demo.model.User;
import sessions.demo.repository.UserRepository;

import java.util.List;

/**
 * @author Ali Dehghani
 */
@RestController
@RequestMapping("/users")
public class UserController {

    private final UserRepository userRepository;

    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Only authenticated users can call this service.
     */
    @GetMapping
    @PreAuthorize("isAuthenticated()")
    public List<User> listUsers() {
        return userRepository.findAll();
    }

    /**
     * Only users with ROLE_ADMIN role can create a new user.
     */
    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public User createUser(@RequestBody User user) {
        return userRepository.save(user);
    }
}
